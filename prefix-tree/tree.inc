L_OpenGL:
	db "OpenGL is cross-platform graphics API.", 0

G_OpenGL:
	dq "L"
	dq L_OpenGL

n_OpenGL:
	dq "G"
	dq G_OpenGL

e_OpenGL:
	dq "n"
	dq n_OpenGL

p_OpenGL:
	dq "e"
	dq e_OpenGL

O_OpenGL:
	dq "p"
	dq p_OpenGL

M_Metal:
	dq "e"
	dq e_Metal

e_Metal:
    dq "t"
    dq t_Metal

t_Metal:
    dq "a"
    dq a_Metal

a_Metal:
    dq "l"
    dq l_Metal

l_Metal:
    db "Metal is Apple-specific graphics API.", 0

root:
    dq "O"
    dq O_OpenGL
    dq "M"
    dq M_Metal
