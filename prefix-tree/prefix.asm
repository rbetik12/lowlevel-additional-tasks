%include "linux64.inc"
%include "tree.inc"

%define BUFFER_SIZE 255

section .data
input_buffer: times BUFFER_SIZE db 0
error_string: db "Element with such key doesn't exists!", 0xA, 0

section .text
global _start

_start:
    mov rdi, input_buffer
    mov rsi, BUFFER_SIZE
    call read_line

    mov rax, root ; stores current node address
    mov rbx, [rax] ; stores element to compare
    mov r8, input_buffer
    xor rcx, rcx ; stores key chars
    xor rdx, rdx ; counter
    xor r9, r9 ; key length

    push rax
    mov rdi, r8
    call string_length
    mov r9, rax
    pop rax

    mov cl, [r8]
    mov rdi, rcx

    cmp rbx, rcx
    je .start_finding_first

    mov rbx, [rax + 16]
    cmp rbx, rcx
    je .start_finding_second

    .error:
    mov rdi, error_string
    call print_string
    exit

    .start_finding_first:
    inc r8  ; increments key address
    push r8 ; saves it in stack
    sub r8, input_buffer ; puts current key index to r8
    cmp r8, r9
    jae .okay
    pop r8 ; loads r8 from stack

    mov cl, [r8] ; moves to rcx char from key

    mov rbx, [rax + 8] ; moves to rbx address of next node
    mov rax, rbx ; moves next node address to rax
    mov rbx, [rax] ; moves node key char to rbx
    mov rdi, rbx

    push rcx
    push rax
;    call print_char
    pop rax
    pop rcx

    cmp rbx, rcx
    je .start_finding_first

    jmp .error

    .start_finding_second:
        inc r8
        push r8
        sub r8, input_buffer
        cmp r8, r9
        jae .okay
        pop r8

        mov cl, [r8]

        mov rbx, [rax + 24]
        mov rax, rbx
        mov rbx, [rax]
        mov rdi, rbx

        push rcx
        push rax
    ;    call print_char
        pop rax
        pop rcx

        cmp rbx, rcx
        je .start_finding_first


    .okay:
    mov rbx, [rax + 8]
    mov rax, rbx
    mov rdi, rax

    push rax
    call string_length
    cmp rax, 1
    je .error
    pop rax
    call print_string
    call print_newline
    exit

