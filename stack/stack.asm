%include "linux64.inc"
%define BUFFER_SIZE 1000000

global _start

section .data
buffer: times BUFFER_SIZE db 0
proc_filename: db 'file.txt', 0
debug_string: db 'Debug', 0

filename: db "/proc/self/maps", 0
test_number: db "12345", 0

section .bss
text resb BUFFER_SIZE
address resb BUFFER_SIZE

section .text

parse_uint:
    push rcx
    xor rax, rax
    xor rsi, rsi

    mov r8, 16

    xor rcx, rcx ; string element counter
    xor rdx, rdx

    .loop:
    mov sil, [rdi+rcx]

    inc rcx

    cmp sil, 0x30 ; checks for ascii code in range [0x30, 0x46]
    jl .return

    cmp sil, 0x66
    jg .return

    cmp sil, 0x39
    jle .convert_dec

    cmp sil, 0x61
    jae .convert_hex

    jmp .return

    .convert_dec:

    sub sil, 0x30 ; converts to number
    mul r8
    add rax, rsi
    jmp .loop

    .convert_hex:

    sub sil, 0x57 ; converts to number
    mul r8
    add rax, rsi
    jmp .loop

    .return:
    mov rdx, rcx

    pop rcx
    ret


skip_to_third_line:
    push rbx
    xor rbx, rbx ; \n counter
    push rcx
    mov rcx, 0 ; index counter
    xor rax, rax

    .loop:

    mov al, byte [rdi + rcx]
    inc rcx

    cmp al, 0xA
    je .is_newline
    jmp .loop

    .is_newline:
    inc rbx
    cmp rbx, 3

    je .end
    jmp .loop

    .end:
    mov rax, rcx

    pop rcx
    pop rbx

    ret

; rdi - pointer to string
; rsi - pointer to buffer for copying
copy_address:
    push rcx
    push rbx
    xor rbx, rbx; copy counter
    call string_length

    .copy_loop:
    mov     cl, [rdi]
    inc     rdi
    mov     [rsi], cl
    inc     rsi
    inc     rbx
    cmp     rbx, 12
    jne     .copy_loop
    jmp     .end


    .end:
    mov byte [rsi], 0
    pop rbx
    pop rcx
    ret



_start:
    mov rax, SYS_OPEN
    mov rdi, filename
    mov rsi, O_RDONLY
    mov rdx, 0
    syscall

    push rax
    mov rdi, rax
    mov rax, SYS_READ
    mov rsi, text
    mov rdx, BUFFER_SIZE
    syscall


    mov rdi, text

    call skip_to_third_line

    lea rdi, [text + rax]
    add rdi, 13

;    call print_string

    mov rsi, address
    call copy_address

    mov rdi, address
    call print_string

    call print_newline

    mov rdi, address
    call parse_uint

    mov rdi, rax
    sub rdi, rsp

    mov rax, rdi
    mov r9, 8
    mov rdx, 0
    div r9
    mov rdi, rax
    call print_uint
    call print_newline
    xor rbx, rbx

    .loop:

    pop rax

    inc rbx
    mov rdi, rbx
    call print_uint

    call print_newline

    jmp .loop


    mov rax, SYS_CLOSE
    pop rdi
    syscall

    exit